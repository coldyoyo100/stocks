package com.pretest.stocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class StocksApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(StocksApplication.class, args);
	}

}
