package com.pretest.stocks.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
	
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http
			.headers().frameOptions().disable()
			.and()
			.csrf().disable()
			.authorizeHttpRequests((requests) -> requests
				.antMatchers("/**").permitAll()
//				.requestMatchers(req-> req.getRequestURI().contains("/save")).hasAuthority("ADMIN")
//				.anyRequest().authenticated()
			)
			.sessionManagement()
				.maximumSessions(1);
		
		return http.build();
	}
	
}
