package com.pretest.stocks.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pretest.stocks.dto.StocksDto;
import com.pretest.stocks.model.Dashboard;
import com.pretest.stocks.model.Stocks;
import com.pretest.stocks.service.StocksService;

@Controller
public class StocksController {
	
	@Autowired StocksService service;
	
	@RequestMapping("/")
	public String home(@ModelAttribute("stocks") Dashboard dashboard, BindingResult bindingResult, HttpServletRequest request, Model model) {
		model.addAttribute("msgAlert","Welcome");
		model.addAttribute("successAlert", true);
		return "webpages/dashboard"; 
	}
	
	@RequestMapping("/insert") 
	public String insert(@ModelAttribute("stocks") Dashboard dashboard, BindingResult bindingResult, HttpServletRequest request, Model model) {
		ObjectMapper mapper = new ObjectMapper();
		if(dashboard.getSubmitFlag() == 1) {
			List<Stocks> stocks = service.fetchAllStocks();
			
			if(stocks.size() > 0) {
				dashboard.setListCsv(stocks);
				model.addAttribute("msgAlert","Data refresh! Total Stock: " + stocks.size());
				model.addAttribute("successAlert", true);
			}else {
				model.addAttribute("msgAlert","No Data Found");
				model.addAttribute("successAlert", true);
			}
		}else {
			String path = dashboard.getPath();
			String fileName = dashboard.getFileName();
			
			if(!StringUtils.isBlank(dashboard.getPath()) && !StringUtils.isBlank(dashboard.getPath()) ) {
				
				List<StocksDto> dto = service.insertCsv(path, fileName); 
				List<Stocks> stocksModel = new ArrayList<Stocks>();
				dto.forEach(obj -> {
					stocksModel.add(mapper.convertValue(obj, Stocks.class) );
				});
				
				dashboard.setListCsv(stocksModel);
			}else {
				model.addAttribute("msgAlert", "Please input Path and Filename");
				model.addAttribute("errorAlert", true);
			}
		}
	    return "webpages/dashboard"; 
	} 
	
	
	@PostMapping("/insert") 
	public ResponseEntity<Object> postInsert(@RequestBody Map<String, String> json){
		String fileName = json.get("filename");
		String filePath = json.get("path");
		if(!fileName.contains(".csv")) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please input .csv extention file.");
		
		List<StocksDto> listDto = new ArrayList<StocksDto>();
		
		if(!StringUtils.isBlank(filePath) && !StringUtils.isBlank(fileName) ) {
			listDto = service.insertCsv(filePath, fileName) ;
		}
		
		if(listDto.size() == 0) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No Data Insert!");
		
	    return ResponseEntity.status(HttpStatus.OK).body(listDto);
	} 
	
	@GetMapping("/getAllStock") 
	public ResponseEntity<List<StocksDto>> inquiryAllStock() {
		List<StocksDto> dto = service.getAllStocksInfo();
		return ResponseEntity.status(HttpStatus.OK).body(dto); //new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@GetMapping("/getStock/{stock}") 
	public ResponseEntity<StocksDto> inquiryStock(@PathVariable("stock") String stock) {
		StocksDto dto = service.getStockInfo(stock);
		return ResponseEntity.status(HttpStatus.OK).body(dto); //new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@GetMapping("/findStock/{stock}") 
	public ResponseEntity<List<StocksDto>> findStock(@PathVariable("stock") String stock) {
		List<StocksDto> listDto = service.findStockInfo(stock);
		return ResponseEntity.status(HttpStatus.OK).body(listDto);
	}
	
	@GetMapping("/delStock/{stock}") 
	public ResponseEntity<String> deleteStock(@PathVariable("stock") String stock) {
		String retval = service.deleteStockByCode(stock);
		return ResponseEntity.status(HttpStatus.OK).body(retval);
	}
	
	@PostMapping("/updateStock/{stock}") 
	public ResponseEntity<String> updateStock(@PathVariable("stock") String targetStock, @RequestBody Stocks newInfo) {
		
		String retval = service.updateStockByCode(targetStock, newInfo);
		return ResponseEntity.status(HttpStatus.OK).body(retval);
	}
}
