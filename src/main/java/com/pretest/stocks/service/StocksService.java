package com.pretest.stocks.service;

import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pretest.stocks.dto.StocksDto;
import com.pretest.stocks.entity.StocksEntity;
import com.pretest.stocks.model.Stocks;
import com.pretest.stocks.repo.StocksRepo;

@Service
public class StocksService {
	private static final Logger LOG = LoggerFactory.getLogger(StocksService.class);
	
	@Autowired StocksRepo stocksRepo;
	private ObjectMapper mapper;
	
	public StocksService() {
		mapper = new ObjectMapper();
	}
	
	@Transactional
	public List<StocksDto> insertCsv(String path, String fileName) {
		String csvFile = path + fileName;
		List<StocksEntity> listStocks = new ArrayList<StocksEntity>();
		List<StocksDto> retval = new ArrayList<StocksDto>();
		Integer index = 0;
		
		try (Reader reader = new FileReader(csvFile); CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT)) {

			for (CSVRecord csvRecord : csvParser) {
				if (index == 0) {
					index++;
					continue;
				}
				StocksEntity obj = new StocksEntity();
				obj.setCode(csvRecord.get(1));
				obj.setName(csvRecord.get(2));
				obj.setMkt(csvRecord.get(3));
				obj.setSt(csvRecord.get(4));
				obj.setPrev(Integer.parseInt(csvRecord.get(5)));
				obj.setOpen(Double.parseDouble(csvRecord.get(6)));
				obj.setLast(Double.parseDouble(csvRecord.get(7)));
				obj.setHigh(Double.parseDouble(csvRecord.get(8)));
				obj.setLow(Double.parseDouble(csvRecord.get(9)));
				obj.setbVol(Integer.parseInt(csvRecord.get(10)));
				obj.setBid(Double.parseDouble(csvRecord.get(11)));
				obj.setOffer(Double.parseDouble(csvRecord.get(12)));
				obj.setoVol(Integer.parseInt(csvRecord.get(13)));
				obj.setPlusNmin(Integer.parseInt(csvRecord.get(14)));
				obj.setPercentage(Double.parseDouble(csvRecord.get(15)));
				obj.setlVol(Integer.parseInt(csvRecord.get(16)));
				obj.setFreq(Integer.parseInt(csvRecord.get(17)));
				obj.setVol(Integer.parseInt(csvRecord.get(18)));
				obj.setVal(Double.parseDouble(csvRecord.get(19)));
				listStocks.add(obj);
				index++;
			}
			
			if(listStocks.size() > 0) stocksRepo.saveAll(listStocks);
			
			listStocks.forEach(obj -> {
				retval.add(mapper.convertValue(obj, StocksDto.class) );
			});

		} catch (Exception e) {
			System.out.println("error index:" + index);
			e.printStackTrace();
		}

		return retval;
	}
	
	public List<StocksDto> getAllStocksInfo(){
		List<StocksDto> retval = new ArrayList<StocksDto>();
		List<Map<String, Object>> customDto = stocksRepo.customNativeQueryFindAll();
		
		if(customDto.size() > 0 ) {
			for(Map<String, Object> objMap : customDto) {
				
				Map<String, Object> temp = new HashMap<String, Object>();
				
				temp.putAll(objMap.entrySet()
					.stream()
					.filter(key -> "code".equals(key.getKey()) )
					.collect(Collectors.toMap(key -> key.getKey(), val -> val.getValue()) ) );
				
				temp.putAll(objMap.entrySet()
					.stream()
					.filter(key -> "name".equals(key.getKey()) )
					.collect(Collectors.toMap(key -> key.getKey(), val -> val.getValue().toString().trim()) ) );
					
				retval.add( mapper.convertValue(temp, StocksDto.class));
			}
		}else return null;
		
		return retval;
	}
	
	public StocksDto getStockInfo(String stock) {
		StocksEntity entity = stocksRepo.findByCode(stock.toUpperCase());
		StocksDto dto = mapper.convertValue(entity, StocksDto.class);
		return dto;
	}

	public List<StocksDto> findStockInfo(String stock) {
		List<StocksEntity> entity = stocksRepo.findByCodeLike("%"+stock.toUpperCase()+"%");
		List<StocksDto> retval = new ArrayList<StocksDto>();
		entity.forEach(val -> {
			retval.add(mapper.convertValue(val, StocksDto.class) );
		});
		return retval;
	}

	@Transactional
	public String deleteStockByCode(String stock) {
		StocksEntity entity = stocksRepo.findByCode(stock.toUpperCase());
		if(entity == null) return "Stock Not Found!";
		else stocksRepo.delete(entity);
		
		return stock.toUpperCase() + " Deleted!";
		
	}

	@Transactional
	public String updateStockByCode(String targetStock, Stocks newInfo) {
		Boolean valid = true;
		StocksEntity entity = stocksRepo.findByCode(targetStock.toUpperCase());
		try{
			if(! StringUtils.isEmpty( newInfo.getCode() ) ) entity.setCode(newInfo.getCode());
			if(! StringUtils.isEmpty( newInfo.getName() ) ) entity.setName(newInfo.getName());
			if(! StringUtils.isEmpty( newInfo.getMkt() ) ) entity.setMkt(newInfo.getMkt());
			if(newInfo.getPrev() != null) entity.setPrev(newInfo.getPrev());
			if(newInfo.getOpen() != null) entity.setOpen(newInfo.getOpen());
			if(newInfo.getLast() != null) entity.setLast(newInfo.getLast());
			if(newInfo.getHigh() != null) entity.setHigh(newInfo.getHigh());
			if(newInfo.getLow() != null) entity.setLow(newInfo.getLow());
			if(newInfo.getbVol() != null) entity.setbVol(newInfo.getbVol());
			if(newInfo.getBid() != null) entity.setBid(newInfo.getBid());
			if(newInfo.getOffer() != null) entity.setOffer(newInfo.getOffer());
			if(newInfo.getoVol() != null) entity.setoVol(newInfo.getoVol());
			if(newInfo.getPlusNmin() != null) entity.setPlusNmin(newInfo.getPlusNmin());
			if(newInfo.getPercentage() != null) entity.setPercentage(newInfo.getPercentage());
			if(newInfo.getlVol() != null) entity.setlVol(newInfo.getlVol());
			if(newInfo.getFreq() != null) entity.setFreq(newInfo.getFreq());
			if(newInfo.getVol() != null) entity.setVol(newInfo.getVol());
			if(newInfo.getVal() != null) entity.setVal(newInfo.getVal());
		}catch(Exception e) {
			valid = false;
			e.printStackTrace();
		}
		
		if(valid) stocksRepo.save(entity);
		
		return  targetStock + (valid ? " Update Success" : "  Update Failed");
	}

	public List<Stocks> fetchAllStocks() {
		List<StocksEntity> allStocks = stocksRepo.findAll();
		List<Stocks> retval = new ArrayList<Stocks>();
		
		allStocks.forEach(obj -> {
			retval.add( mapper.convertValue(obj, Stocks.class) );
		});
		
		return retval;
	}
}
