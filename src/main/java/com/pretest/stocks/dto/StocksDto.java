package com.pretest.stocks.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = Include.NON_NULL)
public class StocksDto implements Serializable{
	
	private static final long serialVersionUID = -5357329682939441185L;
	@JsonIgnore public Integer id;
	public String code;
	public String name;
	public String mkt;
	public String st;
	public Integer prev;
	public Double open;
	public Double last;
	public Double high;
	public Double low;
	public Integer bVol;
	public Double bid;
	public Double offer;
	public Integer oVol;
	public Integer plusNmin;
	public Double percentage;
	public Integer lVol;
	public Integer freq;
	public Integer vol;
	public Double val;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMkt() {
		return mkt;
	}
	public void setMkt(String mkt) {
		this.mkt = mkt;
	}
	public String getSt() {
		return st;
	}
	public void setSt(String st) {
		this.st = st;
	}
	public Integer getPrev() {
		return prev;
	}
	public void setPrev(Integer prev) {
		this.prev = prev;
	}
	public Integer getbVol() {
		return bVol;
	}
	public void setbVol(Integer bVol) {
		this.bVol = bVol;
	}
	public Integer getoVol() {
		return oVol;
	}
	public void setoVol(Integer oVol) {
		this.oVol = oVol;
	}
	public Integer getPlusNmin() {
		return plusNmin;
	}
	public void setPlusNmin(Integer plusNmin) {
		this.plusNmin = plusNmin;
	}
	public Double getPercentage() {
		return percentage;
	}
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	public Integer getlVol() {
		return lVol;
	}
	public void setlVol(Integer lVol) {
		this.lVol = lVol;
	}
	public Integer getFreq() {
		return freq;
	}
	public void setFreq(Integer freq) {
		this.freq = freq;
	}
	public Integer getVol() {
		return vol;
	}
	public void setVol(Integer vol) {
		this.vol = vol;
	}
	public Double getOpen() {
		return open;
	}
	public void setOpen(Double open) {
		this.open = open;
	}
	public Double getLast() {
		return last;
	}
	public void setLast(Double last) {
		this.last = last;
	}
	public Double getHigh() {
		return high;
	}
	public void setHigh(Double high) {
		this.high = high;
	}
	public Double getLow() {
		return low;
	}
	public void setLow(Double low) {
		this.low = low;
	}
	public Double getBid() {
		return bid;
	}
	public void setBid(Double bid) {
		this.bid = bid;
	}
	public Double getOffer() {
		return offer;
	}
	public void setOffer(Double offer) {
		this.offer = offer;
	}
	public Double getVal() {
		return val;
	}
	public void setVal(Double val) {
		this.val = val;
	}
	
	
}
