package com.pretest.stocks.repo.redis;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pretest.stocks.entity.StocksEntity;

public interface StocksRedis {
	
	StocksEntity findByCode(String code);
	
	List<StocksEntity> findByCodeLike(String code);
	
	Boolean saveStocks(StocksEntity stocksEntity);

	List<StocksEntity> getAllRecords();

	Boolean saveAll(String jsonStocks);

	Boolean delete(String code, StocksEntity stocksEntity);

	Boolean update(String code, StocksEntity entity);
}
