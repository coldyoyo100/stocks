package com.pretest.stocks.repo.redis.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.pretest.stocks.entity.StocksEntity;
import com.pretest.stocks.repo.redis.StocksRedis;

@Repository
public class StocksRedisImpl implements StocksRedis{
	
	@Autowired
    private RedisTemplate redisTemplate;
	
	private static final String KEY = "STOCK";
	
	@Override
    public Boolean saveStocks(StocksEntity stocksEntity) {
        try {
            redisTemplate.opsForHash().put(KEY, stocksEntity.getCode().toString(), stocksEntity);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
	
	@Override
	public Boolean saveAll(String jsonStocks) {
		try{
			redisTemplate.opsForValue().set(KEY, jsonStocks);
		}catch (Exception e) {
            e.printStackTrace();
            return false;
        }
		return true;
	}
	
	@Override
	public StocksEntity findByCode(String code) {
		return (StocksEntity) redisTemplate.opsForHash().get(KEY, code);
	}

	@Override
	public List<StocksEntity> findByCodeLike(String code) {
		return (List<StocksEntity>) redisTemplate.keys(code+"*");
	}

	@Override
	public List<StocksEntity> getAllRecords() {
		return (List<StocksEntity>)redisTemplate.opsForHash().values(KEY);
	}

	@Override
	public Boolean delete(String code, StocksEntity stocksEntity) {
		try{
			redisTemplate.opsForHash().delete(KEY, code, stocksEntity);
		}catch (Exception e) {
            e.printStackTrace();
            return false;
        }
		return true;
	}

	@Override
	public Boolean update(String code,StocksEntity stocksEntity) {
		try{
			redisTemplate.opsForHash().put(KEY, code, stocksEntity);
		}catch (Exception e) {
            e.printStackTrace();
            return false;
        }
		return true;
	}

	
	
}
