package com.pretest.stocks.repo;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pretest.stocks.entity.StocksEntity;

public interface StocksRepo extends JpaRepository<StocksEntity, Long>{
	
	List<StocksEntity> findAll();
	
	StocksEntity findByCode(String code);
	
	List<StocksEntity> findByCodeLike(String code);
	
	@Query(nativeQuery = true, value = "SELECT * FROM stocks" )
	List<Map<String, Object>> customNativeQueryFindAll();
}
