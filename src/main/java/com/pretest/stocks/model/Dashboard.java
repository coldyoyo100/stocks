package com.pretest.stocks.model;

import java.io.Serializable;
import java.util.List;

public class Dashboard implements Serializable{
	private static final long serialVersionUID = -3959010906323495758L;
	
	private String errorMsg;
	private Integer submitFlag;
	
	private String path;
	private String fileName;
	
	private List<Stocks> listCsv;
	
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public Integer getSubmitFlag() {
		return submitFlag;
	}
	public void setSubmitFlag(Integer submitFlag) {
		this.submitFlag = submitFlag;
	}
	public List<Stocks> getListCsv() {
		return listCsv;
	}
	public void setListCsv(List<Stocks> listCsv) {
		this.listCsv = listCsv;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
