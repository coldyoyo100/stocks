# Pretest tech Stocks


==>> Sample CSV File Included In Resource Folder : StocksData.csv   <<== 

Default URL: http://localhost:9999/stocks/
Redis URL: http://localhost:9999/stocks/redis/			=> All API are identical as Default URL and Documentation

DB Connection - MySql(XAMPP) - not included
----------------------------
port 3306
username root
no pass

##########################################################

==>> MAKSE SURE MySQL Server already RUNNING FIRST before running exec JAR <<==

##########################################################
1. Open Terminal,  
2. Set location to [target] folder with "CD" command
3. Type "java- jar filename-exec.jar" then enter

Access Frontend - only for insert sample data and data presentation
---------------
url: localhost:9999/stocks


################## API DOC ##################

Insert - POST
--------
localhost:9999/stocks/insert
####
jsonBody:
	{
		"path": "C:\\Users\\Documents\\STSWorkspace\\stock\\src\\main\\java\\",
		"filename": "StocksData.csv"
	}

Select - GET
--------
localhost:9999/stocks/getAllStock				=> return All Stock Code & Stock Name only
####
localhost:9999/stocks/getStock/{stockName}			=> return Single Stock Info
####
localhost:9999/stocks/findStock/{stockName}			=> Search Stock Code with LIKE function



Update - POST
---------
localhost:9999/stocks/updateStock/{stockName}
####
Json Body:
	{
		"code": "String",
		"name": "String",
		"mkt": "String",
		"st": "String",
		"prev": Integer,
		"open": Double,
		"last": Double,
		"high": Double,
		"low": Double,
		"bVol": Integer,
		"bid": Double,
		"offer": Double,
		"oVol": Integer,
		"plusNmin": Integer,
		"percentage": Double,
		"lVol": Integer,
		"freq": Integer,
		"vol": Integer,
		"val": Double
	}

Delete - GET
---------
localhost:9999/stocks/delStock/{stockName} 			=> Delete Single Stock by Stock Code
